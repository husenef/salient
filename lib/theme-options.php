<?php

if (!defined('ABSPATH')) {
    exit; // disable direct access
}
if (!class_exists('Site_Options')) :

    class Site_Options {

        var $siteOptions = null;
        var $optionName = 'siteOptions';
        var $options = array();

        public static function init() {
            $siteOptions = new self();
        }

        function __construct() {
            $this->add_script();
            $this->save_options();
            $this->set_options();
            $this->pages();
        }

        function pages() {
            $html = "";
            $html .= "<h3>Site Options</h3>";
            $html .= "<form action='' method='post'>";

            $html .="<h3>Home Page</h3><hr/>";
            $html .="<table class='form-table'><tbody><tr>";
            $html .="<th scope='row'><label for='cat'>Product Category</label></th>";
            $html .="<td>";
            $html .= $this->getProductCat($this->options->prodCats);
            $html .="</td></tr>";

            $html .="</tr></tbody></table>";

            $html .="<p class='submit'><input type='submit' value='Update Options' class='button button-primary' id='submit'></p> ";
            $html .="</form>";
            echo $html;
        }

        function save_options() {
            if ($_POST):
                print_r($_POST);
                $data = json_encode($_POST);
                if (get_option($this->optionName) !== false) {
                    update_option($this->optionName, $data);
                } else {
                    $deprecated = null;
                    $autoload = 'no';
                    add_option($this->optionName, $data, $deprecated, $autoload);
                }
                $_SESSION['message'] = array('c' => '1', 'm' => 'Data Sudah di simpan');
                $this->redirect(admin_url('themes.php?page=theme_option.php'));
            endif;
        }

        function set_options() {
            $options = get_option($this->optionName);
            $this->options = json_decode($options);
            return $this;
        }

        function add_script() {
//            wp_enqueue_media();
        }

        function redirect($url) {
            echo "<script type='text/javascript'>window.location = '" . $url . "';</script>";
        }

        private function getProductCat($idProduct = null) {
            $taxonomy = 'product_cat';
            $orderby = 'name';
            $show_count = 0; // 1 for yes, 0 for no
            $pad_counts = 0; // 1 for yes, 0 for no
            $hierarchical = 1; // 1 for yes, 0 for no
            $title = '';
            $empty = 0;

            $args = array(
                'taxonomy' => $taxonomy,
                'orderby' => $orderby,
                'show_count' => $show_count,
                'pad_counts' => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li' => $title,
                'hide_empty' => $empty
            );
            $all_categories = get_categories($args);
//            print_r($all_categories);
            $return = "<select id='cat' name='prodCats' required='required'>";
            $return .="<option value=''>--select category--</option>";
            foreach ($all_categories as $value) {
                $selected = ($idProduct == $value->term_id) ? "selected='selected'" : "";
                $return .="<option value='$value->term_id' $selected>$value->name</option>";
            }
            $return .= "</select>";
            return $return;
        }

    }

    endif;

function themeoption() {
    add_theme_page('Site Option', 'Site Options', 'manage_options', 'theme_option.php', array('Site_Options', 'init'));
}

add_action('admin_menu', 'themeoption');

/* register script&css */

add_action('wp_enqueue_scripts', 'register_my_script');

function register_my_script() {

    wp_register_style('esteinborn_css', get_template_directory_uri() . '/vendor/esteinborn/css/listnav.css');
    wp_enqueue_style('esteinborn_css');
    wp_register_script('esteinborn_js', get_template_directory_uri() . '/vendor/esteinborn/jquery-listnav.min.js', array(), false, true);
    wp_enqueue_script('esteinborn_js');

    wp_register_style('slick_css', get_template_directory_uri() . '/vendor/slick/slick.css');
    wp_enqueue_style('slick_css');
    wp_register_script('slick_js', get_template_directory_uri() . '/vendor/slick/slick.min.js', array(), false, true);
    wp_enqueue_script('slick_js');
    wp_register_script('extra.js', get_template_directory_uri() . '/js/extra.js', array(), false, true);
    wp_enqueue_script('extra.js');
}
