<?php

if (!defined('ABSPATH')) {
    exit; // disable direct access
}

add_shortcode("allartist", 'all_artists');

function all_artists() {

    $taxonomies = get_terms('project-type', array('hide_empty' => false));
    wp_reset_query();
    $article = array();
    $html = "<ul id='atist-list' class='allartis'>";
    foreach ($taxonomies as $taxonomie) {

        $catname = $taxonomie->name;
//        $alfabet = substr($catname, 0, 1);
        $posts = query_posts(
                array(
                    'post_type' => 'portfolio',
                    'post_status' => 'publish',
                    'posts_per_page' => 1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'project-type',
                            'field' => 'term_id',
                            'terms' => $taxonomie->term_id,
                        )
                    )
                )
        );
        if (!empty($posts)):
            $thumbnail = get_the_post_thumbnail($posts[0]->ID, 'full', array('class' => 'img-responsive'));
            $html .= "<li data-img='$thumbnail'>$catname</li>";
//            $posts[0]->thumbnail = get_the_post_thumbnail($posts[0]->ID, 'full', array('class' => 'img-responsive'));
//            $article[$catname] = $posts;
        else:
            $html .= "<li>$catname</li>";
//            $article[$catname] = null;
        endif;

        wp_reset_postdata();
    }
//    $article = json_encode($article);
//    echo "<pre>";
//    print_r($article);
//    echo "</pre>";
    $html .= "</ul>";
    return $html;
}

/* latest post */
add_shortcode("latest_post", 'latestPost');

function latestPost($attr, $content) {
    $posts = get_posts(array('posts_per_page' => 6));
    $html = '<div class="home-wrap latest_post">'
            . '    <div class="container main-post">';
    $html .= "<div class='row'>";
    $html .= "<div class='clear-both'>";
    foreach ($posts as $key => $post) :
        setup_postdata($post);
        $cat = wp_get_post_categories(get_the_ID());
        $html .= "<div class='post-$key span_4'>";
        $html .= "<span>$cat->name</span>";
        $html .= get_the_post_thumbnail(get_the_ID(), 'thumbnail', array('class' => 'img-responsive'));
        $html .= "</div>";
    endforeach;
    $html .= "</div>";
    $html .="</div>";
    $html .= "</div>"
            . "</div>";
    echo $html;
}
