<div class="home-shop home-wrap">
    <div class="container shop-content">
        <div class="row">

            <div class="vc_span12 wpb_column column_container col no-extra-padding has-animation" data-hover-bg="" data-animation="fade-in-from-bottom" data-delay="2" style="opacity: 0;">
                <?php
//            $options = get_option('siteOptions');
//            $options = json_decode($options);
//            $options->prodCats;
                ?>                
                <h2>COLLECT ART!</h2>
                <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry</h3>
                <div class="clear-both">
                    <?php
                    $n = 1;
                    for ($i = 1; $i <= 6; $i++) {
                        ?>
                        <div class="col span_3 product clear-both div-<?php echo $i ?>">
                            <div class="row">
                                <img src="<?php echo get_template_directory_uri() ?>/img/product/product.png" class=""/>
                                <h4>Anisa Kenyut</h4>
                                <p><strong>"Alis Padi"</strong> - 2014</p>
                                <p><small>(Paddy Eyebrow)</small></p>
                                <p><small>140 x 140</small></p>
                                <p><small>FIxed Media on Canvas</small></p>
                                <p style="text-align: right;">RP</p>
                            </div>
                        </div>
                        <?php
                        if ($i % 4 == 0) {
                            echo "</div><div class='clear-both clear-both-$n'>";
                            $n++;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>