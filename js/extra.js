(function ($) {
    $('#product-collected').slick({
        slidesToShow: 6,
        slidesToScroll: 6,
        prevArrow: '.prev',
        nextArrow: '.next',
        responsive: [
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
    });
    //artistPage
    if ($('#atist-list').length > 0) {
        $("#atist-list").listnav({
            flagDisabled: false,
            includeAll: true,
            includeNums: false,
            showCounts: false
        });
    }

    $('#artistPage .nav > a.prev').on('click', function (e) {
        e.preventDefault();
        $('.ln-letters .ln-selected').prev().click();
        return false;
    });
    $('#artistPage .nav > a.next').on('click', function (e) {
        e.preventDefault();
        $('.ln-letters .ln-selected').next().click();
        return false;
    });

    var im = '';
    $('ul.allartis').find('li').each(function ($i) {
        if ($(this).data('img') && im !== 'ada') {
            im = 'ada';
            $('.loading').fadeOut('slow');
            $('.target').hide('fast').html($(this).data('img')).fadeIn('slow');
        }
    });
    $('ul.allartis > li').on('hover', function () {
        var img = $(this).data('img');
        var target = $('.target');
        var imgs = (img) ? img : '<p style="text-align:center">Image not Available</p>';
        target.fadeOut('slow', function () {
            target.html(imgs).fadeIn();
        });
    });

})(jQuery);