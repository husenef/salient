<?php
/* template name: Artist */
get_header();
?>
<div class="home-wrap">
    <div class="container">
        <div class="row" id="artistPage">
            <div class="span_6 list col">
                <?php echo (do_shortcode("[allartist]")) ?>
                <div class="clearfix nav">
                    <a class="prev" href="#">&lt; Prev</a>
                    <a class="next" href="#">Next &gt;</a>
                </div>
            </div>
            <div class="span_6 thumb col">
                <div class="target">

                </div>
                <div class="loading clearfix">
                    <div class="pulse-loader">
                        Loading…
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="home-wrap">
    <div class="container main-content">
        <div class="row">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>