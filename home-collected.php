<div class="home-collected home-wrap">

    <div class="full-title clear-fix">
        <div class="container shop-content">
            <div class="row">
                <div class="vc_span12 wpb_column column_container col no-extra-padding has-animation" data-hover-bg="" data-animation="fade-in-from-bottom" data-delay="2" style="opacity: 0;">
                    <h2>RECENTLY COLLECTED</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row" >
            <div class="vc_span12 wpb_column column_container col no-extra-padding has-animation" data-hover-bg="" data-animation="fade-in-from-bottom" data-delay="2" style="opacity: 0;">
                <div id="product-collected">
                    <?php
                    $n = 1;
                    for ($i = 1; $i <= 8; $i++) {
                        ?>
                        <div class="col span_4 product clear-both div-<?php echo $i ?>" style="margin-right: 20px;">
                            <img src="<?php echo get_template_directory_uri() ?>/img/product/product.png" class=""/>
                            <h4>Anisa Kenyut1</h4>
                            <p><strong>"Alis Padi"</strong> - 2014</p>
                            <p><small>140 x 140</small></p>
                            <p style="text-align: right;">RP</p>
                        </div>
                        <?php
//                if ($i % 3 == 0) {
//                    echo "</div><div class='clear-both clear-both-$n'>";
//                    $n++;
//                }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- button -->
    <div class="btn-next">
        <a href="#" class="prev">
            <span>  Prev</span>
        </a>
        <a class="next" href="#">
            <span>Next</span>
        </a>
    </div>
    <!-- next-prev -->
</div>